# klugcar

Small, efficient sidecar to enable intelligent load balancing.

Project goals:

- Be highly efficient
- Extensible
- Require least amount of application changes.

This project uses adr, and a log of all architectural decisions can be found in [doc/adr/](doc/adr)
