# 2. kubernetes-support

Date: 2020-05-10

## Status

Accepted

## Context

Currently doing locality aware load balancing outside of K8s 1.17 requires one to use istio, or implement a considerable amount of custom code.

Locality aware load balancing is relevant for latency sensitive workloads, and to avoid extra charges for inter az traffic.

## Decision

Project will support the two features required locality aware load-balancing - registration and discovery of endpoints.

From Tinder's story to their move to Kubernetes

```
The configuration we came up with was to have an Envoy sidecar alongside each pod that had one route and cluster to hit the local container port. To minimize potential cascading and to keep a small blast radius, we utilized a fleet of front-proxy Envoy pods, one deployment in each Availability Zone (AZ) for each service. These hit a small service discovery mechanism one of our engineers put together that simply returned a list of pods in each AZ for a given service.

```

source: https://medium.com/tinder-engineering/tinders-move-to-kubernetes-cda2a6372f44

Such service discovery mechanism will prioritize AWS Cloud Map for the time being. Klugcar will handle registration and querying AWS Cloud Map. It will also take care of routing logic.

## Consequences

In the future other service discovery registries such as consul will have to be supported. Potentially due to plugins.
